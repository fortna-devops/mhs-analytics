
from typing import List, Dict, Any, Optional
import logging
from ipc_rdb.models import ThresholdLevelEnum
from ipc_rdb.session import Session

from .base_default_calculator import BaseDefaultCalculator
from .alarm_determiners import Result


__all__ = ['DefaultCalculator']


logger = logging.getLogger()


class DefaultCalculator(BaseDefaultCalculator):
    """
    Default calculator.
    """

    _threshold_level_names = {
        ThresholdLevelEnum.YELLOW: 'yellow',
        ThresholdLevelEnum.RED: 'red',
        ThresholdLevelEnum.LOW_YELLOW: 'low_yellow',
        ThresholdLevelEnum.LOW_RED: 'low_red'
    }

    _valid_value_types = (float, int)

    def __init__(self, session: Session, params: Dict[str, Any]):
        super().__init__(session, params)

    def _is_numerical_value(self, values: List[Any]) -> bool:
        """
        Validates value types.
        """

        for value in values:
            if not isinstance(value, self._valid_value_types):
                return False

        return True

    def _calculate_for_metric(self, metric_id: int, metric_data: Dict[str, List[Any]]) -> Result:
        """
        Calculate alarms for one metric.
        """
        result = Result()

        if not metric_data['values']:
            result.add_metric('no_data', self._metric_info.get(metric_id))  # <=========
            return result

        # if not self._is_numerical_value(metric_data['values']):
        #     # If even one value is non-numerical, mark the whole metrics
        #     # as having invalid_value_types
        #     result.add_metric('invalid_value_types', metric)  TODO
        #     return result
            
        determiner = self._alarm_determiner(metric_info=self._metric_info.get(metric_id),   # create an instance of Determiner
                                            metric_data=metric_data,
                                            metric_thresholds=self._thresholds.get(metric_id),
                                            data_aggregation= self._data_aggregation)  

        result.extend(determiner.determine())
        return result

    def _calculate_for_all_metrics(self) -> Result:
        """
        Calculate alarms for all metric_id's in the Threshold table whose
        'analytic' column value equals the '_name' of this object.
        """
        result = Result()

        # All the metrics for which we want to get TS data
        metric_ids = list(self._thresholds.keys())

        # Get TS data
        ts_data_by_metric = self._get_ts_data(metric_ids)

        # Metric ids for which we found TS data
        metric_ids_with_data = list(ts_data_by_metric.keys())

        # Calculate alarms
        for metric_id in metric_ids_with_data:
            result.extend(self._calculate_for_metric(metric_id, ts_data_by_metric.get(metric_id)))

        return result

    def _log_result(self, result: Optional[Result] = None):
        """
        Log results.
        """

        if not result:
            return

        metrics = result.get_metrics('no_threshold_level_names')
        if metrics:
            logger.warning(f"No data were found for the last "
                           f"{self._amount_of_data_to_analyze.seconds} seconds for metrics: {metrics}.")

        metrics = result.get_metrics('no_data')
        if metrics:
            logger.warning(f"No data were found for the last "
                           f"{self._amount_of_data_to_analyze} minutes for the metrics: {metrics}.")

        metrics = result.get_metrics('invalid_value_types')
        if metrics:
            logger.warning(f"Invalid value types were found for the next metrics: {metrics}.")

        alarm_results = []
        name = self.alarm_determiner.__name__
        num_alarms = len(result.get_alarms(name))
        alarm_results.append(f"{name}: {num_alarms}")

        logger.info(f"Metric ids={self.metric_ids}. "
                    f"{', '.join(alarm_results)}.")
 
    def calculate(self):
        """
        Calculates alarms.
        """
        result = self._calculate_for_all_metrics()        
        self._log_result(result)

        alarms = []
        alarms.extend(result.get_alarms(self._alarm_determiner.__name__))
        self._insert_alarms(alarms)
