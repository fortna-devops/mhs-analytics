from abc import ABC, abstractmethod
from typing import Dict, Any, List
from datetime import datetime, timedelta

from ipc_rdb.session import Session
from ipc_rdb.models import ThresholdsModel
from ipc_rdb.models.enums import DataAggregationEnum
from ipc_rdb.utils.data import DataProxy
from sqlalchemy.sql.schema import DEFAULT_NAMING_CONVENTION


__all__ = ['BaseCalculator']


class BaseCalculator(ABC):
    """
    Base calculator.
    """

    # Microsecond is Postgres' highest timestamp resolution.
    # Our TS data are at microsecond resolution.
    # _data_aggregation_map = {
    #     DataAggregationEnum.NO_AGGREGATION: DataProxy.microsecond_sample_rate,
    #     DataAggregationEnum.AVG_PER_MICROSECOND: DataProxy.microsecond_sample_rate,
    #     DataAggregationEnum.AVG_PER_MILLISECOND: DataProxy.microsecond_sample_rate,
    #     DataAggregationEnum.AVG_PER_SECOND: DataProxy.second_sample_rate,
    #     DataAggregationEnum.AVG_PER_MINUTE: DataProxy.minute_sample_rate,
    #     DataAggregationEnum.AVG_PER_HOUR: DataProxy.hour_sample_rate
    # }
    def __init__(self, session: Session, params: Dict[str, Any]):
        self._analytic = params['name']
        self._session = session
        self._alarm_determiner = params['alarm_determiner']
        self._end_datetime: datetime = params['timestamp']
        self._amount_of_data_to_analyze = timedelta(seconds=params['amount_of_data_to_analyze'])    # expressed in seconds

        self._start_datetime: datetime = self._end_datetime - self._amount_of_data_to_analyze

        self._data_aggregation: DataAggregationEnum = params['data_aggregation']

        self._thresholds: Dict[int, Dict[str, List[Any]]] = self.get_threshold_info()
        self._metric_info: Dict[int, Dict[str, Any]] = self.get_metric_info()

        self._data_source_ids = self._get_data_source_ids()

    @property
    def alarm_determiner(self):
        return self._alarm_determiner

    @property
    def metric_ids(self):
        return list(self._thresholds.keys())

    @property
    def data_source_ids(self):
        return self._data_source_ids

    @abstractmethod
    def calculate(self):
        pass

    @abstractmethod
    def _get_thresholds(self) -> List[ThresholdsModel]:
        pass

    @abstractmethod
    def get_threshold_info(self) -> Dict[int, Dict[str, List[Any]]]:
        """
        Get all rows of Thresholds table.
        """
        pass

    @abstractmethod
    def get_metric_info(self) -> Dict[int, Dict[str, Any]]:
        pass

    @abstractmethod
    def _get_data_source_ids(self) -> List[int]:
        pass
