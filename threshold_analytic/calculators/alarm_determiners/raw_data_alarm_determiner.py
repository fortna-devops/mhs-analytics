from typing import  List, Dict
import pandas as pd


from .base_alarm_determiner import BaseAlarmDeterminer

__all__ = ['RawDataAlarmDeterminer']


class RawDataAlarmDeterminer(BaseAlarmDeterminer):
    """
    informative info here
    """

    _alarm_type = 'no_agg_threshold_analytic'


    def determine_alarms_for_threshold(self, 
                                        level_name: str,
                                        violation_column_name:str, 
                                        alarm_column_name:str,
                                        trigger_value_column_name: str,
                                        df: pd.DataFrame) -> pd.DataFrame:
        '''
        Determine alarms for threshold
        '''

        # 'duration_min' and 'samples' are ignored. Hardcode to 1.
        df[alarm_column_name] = df[violation_column_name].apply(lambda val: True if val==1 else False)
        df[trigger_value_column_name] = df['value']

        return df