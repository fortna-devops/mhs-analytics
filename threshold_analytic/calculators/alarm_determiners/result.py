from typing import List, Dict, Any
from collections import defaultdict

from ipc_rdb.models import AlarmsModel


__all__ = ['Result']


class Result:
    """
    Result.
    """

    def __init__(self):
        self._metrics = defaultdict(list)
        self._alarms = defaultdict(list)

    def __repr__(self) -> str:
        return str({
            'metrics': dict(self._metrics),
            'alarms': {key: [alarm.to_dict() for alarm in alarms] for key, alarms in self._alarms.items()}
        })

    @property
    def metric_keys(self) -> List[str]:
        """
        Returns list of metric keys.
        """

        return list(self._metrics.keys())

    @property
    def alarm_keys(self) -> List[str]:
        """
        Returns list of alarm keys.
        """

        return list(self._alarms.keys())

    def get_metrics(self, key: str) -> List[Dict[str, Any]]:
        """
        Returns list of metrics.
        """

        return self._metrics[key]

    def get_alarms(self, key: str) -> List[AlarmsModel]:
        """
        Returns list of alarms.
        """

        return self._alarms[key]

    def add_metric(self, key: str, metric: Dict[str, Any]):
        """
        Adds metric.
        """

        self._metrics[key].append({
            'id': metric['id'],
            'name': metric['name']
        })

    def add_alarm(self, key: str, alarm: AlarmsModel):
        """
        Adds alarm.
        """

        self._alarms[key].append(alarm)

    def extend(self, result: 'Result'):
        """
        Extends result.
        """

        for key in result.metric_keys:
            for metric in result.get_metrics(key):
                self.add_metric(key, metric)

        for key in result.alarm_keys:
            for alarm in result.get_alarms(key):
                self.add_alarm(key, alarm)
