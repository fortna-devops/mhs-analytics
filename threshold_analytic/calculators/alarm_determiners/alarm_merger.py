from typing import List, Optional, Union
from datetime import datetime

from ipc_rdb.models import ColorsEnum

from .sequence import Sequence


__all__ = ['AlarmMerger']


class AlarmMerger:
    """
    Alarm Merger.
    """

    def __init__(self):
        #sequence of alarms currently being built
        self._current_sequence: Optional[Sequence] = None

        #all alarms found so far
        self._sequences: List[Sequence] = []

    @property
    def sequences(self) -> List[Sequence]:
        """
        Returns list of alarms.
        """
        return self._sequences
    
    def interrupt_sequence(self):
        """
        Interrupts sequential alarms.
        """
        if self._current_sequence is None:
            return

        # if we get here, the sequence of alarms has ended.
        self._sequences.append(self._current_sequence)
        self._current_sequence = None

    def add_value(self, criteria_value: float, color: ColorsEnum, value: Union[float, int], timestamp: datetime):
        """
        Adds a value to the sequence currently being built.
        """

        if self._current_sequence is None:
            self._current_sequence = Sequence(criteria_value, color)

        if self._current_sequence.color == color:
            self._current_sequence.add_value(value, timestamp)
            return

        self.interrupt_sequence()
        self._current_sequence = Sequence(criteria_value, color)
        self._current_sequence.add_value(value, timestamp)
