from typing import List, Optional, Union
from datetime import datetime

from ipc_rdb.models import ColorsEnum


__all__ = ['Sequence']


class Sequence:
    """
    Sequence of exceeded values.
    """

    def __init__(self, criteria_value: float, color: ColorsEnum):
        self._criteria_value = criteria_value
        self._color = color
        self._values: List[Union[float, int]] = []
        self._started_at: Optional[datetime] = None
        self._ended_at: Optional[datetime] = None

    def __repr__(self) -> str:
        return str({
            'criteria_value': self._criteria_value,
            'color': self._color,
            'values': self._values,
            'started_at': self._started_at,
            'ended_at': self._ended_at
        })

    @property
    def criteria_value(self) -> float:
        """
        Returns criteria value.
        """

        return self._criteria_value

    @property
    def color(self) -> ColorsEnum:
        """
        Returns color.
        """

        return self._color

    @property
    def values(self) -> List[Union[float, int]]:
        """
        Returns values.
        """

        return self._values

    @property
    def length(self) -> int:
        """
        Returns length.
        """

        return len(self._values)

    @property
    def started_at(self) -> Optional[datetime]:
        """
        Returns started at.
        """

        return self._started_at

    @property
    def ended_at(self) -> Optional[datetime]:
        """
        Returns ended at.
        """
        return self._ended_at

    def add_value(self, value: Union[float, int], timestamp: datetime):
        """
        Adds value.
        """

        self._values.append(value)

        # Reset the _started_at attribute of alarm only if we are beginning a new sequence
        if self._started_at is None:
            self._started_at = timestamp

        # Extend the _ended_at attribute of alarm
        self._ended_at = timestamp
