import logging
from typing import List, Dict, Union, Any, Optional
from statistics import mean
import pandas as pd

from ipc_rdb.models.enums import DataAggregationEnum
from .base_alarm_determiner import BaseAlarmDeterminer


__all__ = ['ThresholdAlarmDeterminer']

logger = logging.getLogger()

class ThresholdAlarmDeterminer(BaseAlarmDeterminer):
    """
    Base alarm processor.
    """

    _alarm_type = 'default_threshold_analytic'

    # # https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases
    # _duration_unit_string_map = {
    #     #DataAggregationEnum.NO_AGGREGATION: 'force error',
    #     DataAggregationEnum.AVG_PER_MICROSECOND: 'U',
    #     DataAggregationEnum.AVG_PER_MILLISECOND: 'L',
    #     DataAggregationEnum.AVG_PER_SECOND: 'S',
    #     DataAggregationEnum.AVG_PER_MINUTE: 'min',
    #     DataAggregationEnum.AVG_PER_HOUR: 'H',
    #     DataAggregationEnum.AVG_PER_DAY: 'D',
    #     DataAggregationEnum.AVG_PER_WEEK: 'W',
    # }

    def __merge_alarms(self,
                       alarm_column: pd.DataFrame,
                       threshold: Dict) -> pd.DataFrame:
        '''
        Merge 2 alarms when the number of non-alarm datapoints between alarms are equal or less than
        alarm ending parameter.
        We track the previous alarm and when the next alarm is encountered, we check if
            1. prev_alarm index is more than 0 and
            2. the 2 alarms are not consecutive, so no need to merge them and
            3. the two alarms are separated by alarm_ending number of non-alarm datapoints,
            4. then alarms could be merged.
        '''
        alarm_merge_column = pd.Series(alarm_column, copy=True)
        if threshold is not None:
            alarm_ending_val = threshold['alarm_ending']
            id = threshold['id'] # note the threshold id, for logging
            if alarm_ending_val is not None: # if value in db is not defined
                prev_alarm = -1
                for current_alarm in range(0, len(alarm_merge_column)):
                    if alarm_merge_column.iloc[current_alarm]:
                        if ((prev_alarm >= 0) and
                                ((current_alarm - prev_alarm) > 1) and
                                ((current_alarm - prev_alarm) <= (alarm_ending_val + 1))):
                            alarm_merge_column.iloc[prev_alarm + 1:current_alarm] = True
                            logger.info(f'merging alarms: threshold_id:{id}, prev_alarm:{prev_alarm}, current_alarm:{current_alarm}')
                        # below lines are good to have for debugging, hence commented out
                        #else:
                        #    logger.info(f'skipping alarms: threshold_id:{id}, current_alarm:{current_alarm},'
                        #               f' prev_alarm:{prev_alarm}, alarm_ending_val:{alarm_ending_val}')
                        prev_alarm = current_alarm
            else:
                logger.warning(f'alarm_ending_val has value NULL')
        else:
            logger.warning(f'threshold has value NULL')
        return alarm_merge_column

    def determine_alarms_for_threshold(self,
                                        level_name: str,
                                        violation_column_name:str, 
                                        alarm_column_name:str,
                                        trigger_value_column_name: str,
                                        alarm_merge_column_name:str,
                                        df: pd.DataFrame) -> pd.DataFrame:
        '''
        Determine alarms for threshold
        '''

        threshold = self._thresholds[level_name]

        if (threshold['samples'] is not None) and (threshold['samples'] > 0):
            if threshold['samples'] >= threshold['violations']:
                df[alarm_column_name] = df[violation_column_name].rolling(threshold['samples'], min_periods=threshold['violations']).sum() >= threshold['violations']
                df[trigger_value_column_name] = df['value'].rolling(threshold['samples'], min_periods=threshold['violations']).mean()
                df[alarm_merge_column_name] = self.__merge_alarms(df[alarm_column_name],threshold)
            else:
                logger.warning(f"Metric_id {self.metric_id}: ({level_name}) violations ({threshold['violations']}) greater than samples ({threshold['samples']}). Skipped.")
        elif (threshold['duration'] is not None) and (threshold['duration'] > 0):
            window_size = f"{threshold['duration']}{self._window_time_unit}"
            df[alarm_column_name] = df[violation_column_name].rolling(f'{window_size}').sum() >= threshold['violations']
            df[trigger_value_column_name] = df['value'].rolling(f'{window_size}').mean()
            df[alarm_merge_column_name] = self.__merge_alarms(df[alarm_column_name], threshold)
        else:
            logger.warning(f"Metric_id {self.metric_id}: ({level_name}) 'duration' ({threshold['duration']}) and 'samples' ({threshold['samples']}) are invalid. Skipped.")
        return df