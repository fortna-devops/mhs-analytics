from abc import abstractmethod
import logging
from typing import List, Dict, Any
from statistics import mean
import pandas as pd

from ipc_rdb.models import AlarmsModel
from ipc_rdb.models import ColorsEnum
from ipc_rdb.models.enums import DataAggregationEnum

from .sequence import Sequence
from .alarm_merger import AlarmMerger
from .result import Result


__all__ = ['BaseAlarmDeterminer']

logger = logging.getLogger()


class BaseAlarmDeterminer:
    """
    Base alarm determiner. Object of this class determines alarms
    for one metric.
    """

    # The type of alarms (alarm_types table) this Determiner produces.
    _alarm_type: str = 'base_default_analytic'

    def __init__(self,
                metric_info: Dict[str, Any], 
                metric_data: Dict[str, List[Any]],
                metric_thresholds: Dict[str, Dict[str, List[Any]]],
                data_aggregation: DataAggregationEnum):

        self._metric_info = metric_info
        self._values = metric_data['values']
        self._timestamps = metric_data['timestamps']
        self._thresholds = metric_thresholds
        self._alarm_merger = AlarmMerger()

        self._data_aggregation = data_aggregation

        # self._duration and self._window_time_unit determines the window size time
        self._window_time_unit = 'S'     # seconds

    @property
    @abstractmethod
    def type_name(self) -> str:
        return self._alarm_type

    @property
    @abstractmethod
    def metric_id(self) -> int:
        return self._metric_info['id']

    @property
    @abstractmethod
    def metric_display_name(self) -> str:
        return self._metric_info['display_name']

    @property
    @abstractmethod
    def data_source_id(self) -> int:
        return self._metric_info['data_source_id']

    @property
    @abstractmethod
    def data_source_name(self) -> str:
        return self._metric_info['data_source_name']

    @property
    @abstractmethod
    def data_source_location(self) -> str:
        return self._metric_info['data_source_location']

    @property
    @abstractmethod
    def component_id(self) -> int:
        return self._metric_info['component_id']

    @property
    @abstractmethod
    def component_name(self) -> str:
        return self._metric_info['component_name']

    @property
    @abstractmethod
    def asset_id(self) -> int:
        return self._metric_info['asset_id']

    @property
    @abstractmethod
    def asset_name(self) -> str:
        return self._metric_info['asset_name']

    @abstractmethod
    def determine_alarms_for_threshold(self, 
                                        level_name: str,
                                        violation_column_name: str,
                                        alarm_column_name: str,
                                        trigger_value_column_name: str,
                                        alarm_merge_column_name: str,
                                        df: pd.DataFrame) -> pd.DataFrame:
        pass

    @abstractmethod
    def __merge_alarms(self,
                       alarm_column: pd.DataFrame,
                       threshold: Dict) -> pd.DataFrame:
        pass

    def _get_alarms(self) -> List[Sequence]:
        return self._alarm_merger.sequences

    def _determine_alarms(self) -> None:
        # for t in self._thresholds:
        #     print(f'---------------_thresholds: {t}:{self._thresholds.get(t)}')

        # Create a dataframe
        dictionary = {'value': self._values, 'timestamp': self._timestamps}
        df = pd.DataFrame.from_dict(dictionary, orient='columns')
        df.set_index('timestamp', inplace=True)

        def merge_alarms(serie, criterion_value):
            if ('red_alarm' in serie) and (serie['red_merge_alarm']):
                self._alarm_merger.add_value(criterion_value, ColorsEnum.RED, serie['red_avg_trig_value'], serie.name)
            elif ('yellow_alarm' in serie) and serie['yellow_merge_alarm']:
                self._alarm_merger.add_value(criterion_value, ColorsEnum.YELLOW, serie['yellow_avg_trig_value'], serie.name)
            elif ('low_red_alarm' in serie) and serie['low_red_merge_alarm']:
                self._alarm_merger.add_value(criterion_value, ColorsEnum.RED, serie['low_red_avg_trig_value'], serie.name)
            elif ('low_yellow_alarm' in serie) and serie['low_yellow_merge_alarm']:
                self._alarm_merger.add_value( criterion_value, ColorsEnum.YELLOW, serie['low_yellow_avg_trig_value'], serie.name)
            else:
                self._alarm_merger.interrupt_sequence()

        # Determine violation for each threshold ( which equals (metric & threshold_level) )
        for level_name in self._thresholds:
            violation_column_name = f'{level_name}' + '_violation'
            alarm_column_name = f'{level_name}' + '_alarm'
            trigger_value_column_name = f'{level_name}' + '_avg_trig_value'
            alarm_merge_column_name = f'{level_name}' + '_merge_alarm'

            df = self.determine_violation_for_threshold(level_name=level_name,
                                                        violation_column_name=violation_column_name, 
                                                        df=df)      # TODO is interpreter smart enough to not pass df?

            self.determine_alarms_for_threshold(level_name=level_name,
                                            violation_column_name=violation_column_name,
                                            alarm_column_name=alarm_column_name,
                                            trigger_value_column_name=trigger_value_column_name,
                                            alarm_merge_column_name=alarm_merge_column_name,
                                            df=df)

        #pd.set_option('display.max_columns', 12)
        #pd.set_option('display.max_rows', None)
        #pd.set_option('display.width', 0)
        #print(df)
        # Merge consecutive alarms once all the thresholds for the metric has been processed
        threshold = self._thresholds[level_name]
        criterion_value = threshold['value']
        #print(f'criterion value : {criterion_value}, level_name: {level_name}')
        df.apply(merge_alarms, criterion_value=criterion_value, axis=1)
        self._alarm_merger.interrupt_sequence()


    def determine_violation_for_threshold(self, 
                                        level_name: str,
                                        violation_column_name: str,
                                        df: pd.DataFrame) -> pd.DataFrame:
        '''
        Determine violation for one threshold level.
        '''
        threshold = self._thresholds[level_name]   

        # Determine violation
        df[violation_column_name] = df['value'].apply(lambda val: 1 if (val >= threshold['value']) else 0)
        return df

    def determine(self) -> Result:
        """
        Determine alarm for a metric.
        """

        self._determine_alarms() 

        sequences = self._get_alarms()
        result = Result()

        for sequence in sequences:
            
            if self.data_source_name == 'Frame' or \
               self.data_source_name == 'Bearing':
                data_source_name = self.data_source_name + f' ({self.data_source_location})'.lower()
            else:
                data_source_name = self.data_source_name

            description = f"{self.asset_name}'s {data_source_name} {self.metric_display_name} value of {mean(sequence.values):5.2f} exceeded {sequence.criteria_value:5.2f}"

            alarm = AlarmsModel(
                metric_id=self.metric_id,
                started_at=sequence.started_at,
                ended_at=sequence.ended_at,
                analytic_name=self.type_name,
                color=sequence.color,
                # threshold_level,
                trigger_value=mean(sequence.values),
                trigger_criteria=sequence.criteria_value,
                static_description = description
            )

            result.add_alarm(self.__class__.__name__, alarm)

        return result
