from abc import abstractmethod
import logging
from typing import List, Sequence, Dict, Any
from datetime import datetime, timedelta
from sqlalchemy.sql.sqltypes import Boolean

from ipc_rdb import Alarms, Thresholds, Metrics
from ipc_rdb.models import AlarmsModel, ThresholdsModel
from ipc_rdb.utils.data import DataProxy
from ipc_rdb.session import Session
from .base_calculator import BaseCalculator

__all__ = ['BaseDefaultCalculator']


logger = logging.getLogger()


class BaseDefaultCalculator(BaseCalculator):
    """
    Base default calculator.
    """

    _analytic_names = 'default_threshold_analytic'
    _data_proxy_timestamp_order = DataProxy.asc_ordering

    def __init__(self, session: Session, params: Dict[str, Any]):
        super().__init__(session, params)

    @abstractmethod
    def calculate(self):
        pass


    def _get_metric_data(self, metric_ids: List[int]) -> Sequence[Dict[str, Any]]:
        """
        Returns metric data.
        """
        data: list = []

        logger.info('Retrieving TS data.....')
        begin_time = datetime.now()

        data += DataProxy(self._session).get_grouped_by_metric_id(
            start_datetime= self._start_datetime,
            end_datetime=self._end_datetime,
            metric_ids=metric_ids,
            data_aggregation=self._data_aggregation,
            opereration_code_id=1,          # <-------------------- TODO
            timestamp_order=self._data_proxy_timestamp_order
        )

        end_time = datetime.now()
        logger.info(f'....back from getting TS data. Took {end_time - begin_time} num data points={len(data)}')

        return data


    def _get_ts_data(self, metric_ids: List[int]) -> Dict[int, Dict[str, List[Any]]]:
        """
        Query the TS data for the provided metric ids, then group the data points and timestamps by metric id.
        Example of return value:
        {
            1: {
                'values': [0.1, 0.2],
                'timestamps': [datetime.datetime(2020, 1, 1, 0, 0, 0), datetime.datetime(2020, 1, 1, 0, 1, 0)]
            },
            2: {
                'values': [-0.1, -0.2],
                'timestamps': [datetime.datetime(2020, 1, 1, 0, 0, 0), datetime.datetime(2020, 1, 1, 0, 1, 0)]
            }
        }
        """

        data: Dict[int, Dict[str, List[Any]]] = {}
        for metric_data in self._get_metric_data(metric_ids):

            if metric_data['metric_id'] not in data:
                data[metric_data['metric_id']] = {'values': [], 'timestamps': []}

            data[metric_data['metric_id']]['values'].append(metric_data['value'])
            data[metric_data['metric_id']]['timestamps'].append(metric_data['timestamp'])

        return data


    def _insert_alarms(self, alarms: List[AlarmsModel]):
        """
        Insert alarms. All the alarms to be inserted MUST
        be on the same metric_id and MUST be listed in 
        chronological order.
        """
        if len(alarms)==0:
            return

        alarms_at_start: List[Boolean]= []
        # "microseconds" is not recognized by timedelta function, 
        # so resort to using the fact that 1000000microsecond=1sec
        if alarms[0].started_at <= (self._start_datetime + 
                                    timedelta(seconds=(self._data_aggregation.value / 1000000))):
            alarms_at_start.append(True)
        else:
            alarms_at_start.append(False)

        for alarm in alarms[1:]:
             alarms_at_start.append(False)
                
        Alarms(self._session).insert_all_with_flags(alarms, alarms_at_start)


    def _get_thresholds(self, analytic, to_analyze:Boolean=True) -> List[ThresholdsModel]:
        """
        Get metric_ids and their thresholds with 'active'==True
        """
        return Thresholds(self._session).get_thresholds(analytic, to_analyze)


    def get_threshold_info(self) -> Dict[int, Dict[str, Any]]:
        """
        Get threshold info (rows) from the Thresholds table.
        Return a dictionary of dictionaries of the form below
        {
            'metric_id1': {
                'yellow': {
                    'value':  <int or float>
                    'active': <bool>
                    'alarm_desc': <string>
                    'violation': <int>
                    'samples': <int>
                    'duration': <int>
                },
                'red': {
                    'value':  <int or float>
                    'active': <bool>
                    'alarm_desc': <string>
                    'violation': <int>
                    'samples': <int>
                    'duration': <int>
                },
                'low_yellow':{
                    ....
                },
                'low_red': { 
                    ...
                }
            },

            'metric_id2': {
                ....                
            },
        }
        """

        # dictionary of ALL metrics, 
        # each metric dictionary contains its thresholds
        all_metrics: Dict[int, Dict[str, Any]] = {}  

        for threshold in self._get_thresholds(self._analytic, True):
            metric_id = threshold.metric_id
            if threshold.metric_id not in all_metrics:
                one_metric: Dict[str, Any] = {}                # dict of thresholds for ONE metric

                one_threshold: Dict[str, Any] = {              # dict of ONE threshold
                    'id': threshold.id,
                    'value': threshold.value,
                    'updated_at': threshold.updated_at,
                    'active': threshold.active,
                    'alarm_desc': threshold.alarm_desc,
                    'operational_type_id': threshold.operational_type_id,
                    'violations': threshold.violations,
                    'samples': threshold.samples,
                    'duration': threshold.duration,
                    'alarm_ending': threshold.alarm_ending
                }
                one_metric[threshold.level_name] = one_threshold

                all_metrics[metric_id] = one_metric
            else:                                   # TODO - add log on duplicate threshold found (row in Thresholds)
                # A dict for metric_id already exists, 
                # update it only if 'active' field of threshold/row being processed is true
                if threshold.active:
                    if threshold.level_name not in all_metrics[metric_id]:
                        # a threshold for this threshold_level does not yet exist, create it
                        one_threshold: Dict[str, Any] = {
                            'id': threshold.id,
                            'value': threshold.value,
                            'updated_at': threshold.updated_at,
                            'active': threshold.active,
                            'alarm_desc': threshold.alarm_desc,
                            'operational_type_id': threshold.operational_type_id,
                            'violations': threshold.violations,
                            'samples': threshold.samples,
                            'duration': threshold.duration,
                            'alarm_ending': threshold.alarm_ending
                        }
                        all_metrics[metric_id][threshold.level_name] = one_threshold
                    else:
                        # a threshold for this threshold_level already exist,
                        # update it if 'updated_at' is more recent. 
                        # For case of equal timestamps, pick one with higher threshold_id
                        # because thresholds is sorted by id in asc order
                        if all_metrics[metric_id][threshold.level_name]['updated_at'] <= threshold.updated_at:
                            all_metrics[metric_id][threshold.level_name]['id'] = threshold.id
                            all_metrics[metric_id][threshold.level_name]['value'] = threshold.value
                            all_metrics[metric_id][threshold.level_name]['updated_at'] = threshold.updated_at
                            all_metrics[metric_id][threshold.level_name]['active'] = threshold.active
                            all_metrics[metric_id][threshold.level_name]['alarm_desc'] = threshold.alarm_desc
                            all_metrics[metric_id][threshold.level_name]['operational_type_id'] = \
                                                                                        threshold.operational_type_id

                            all_metrics[metric_id][threshold.level_name]['violations'] = threshold.violations
                            all_metrics[metric_id][threshold.level_name]['samples'] = threshold.samples
                            all_metrics[metric_id][threshold.level_name]['duration'] = threshold.duration
                            all_metrics[metric_id][threshold.level_name]['alarm_ending'] = threshold.alarm_ending
                else:
                    # The threshold being processed has 'active'==false, don't do anything
                    pass
        return all_metrics


    def get_metric_info(self) -> Dict[int, Dict[str, Any]]:
        """
        """
        metric_id_list = list(self._thresholds.keys())
        metric_list = Metrics(self._session).get_data_source_and_metric_ids(metric_id_list)
        all_metric_info: Dict[int, Dict[str, Any]] = {}  # dictionary of ALL metrics

        for metric in metric_list:
            metric_info: Dict[str, Any] = {
                'id': metric.id,
                'name': metric.name,
                'data_source_id': metric.data_source_id,
                'display_name': metric.display_name,
                'units': metric.units,
                'min_value': metric.min_value,
                'max_value': metric.max_value,
                'raw_data_table': metric.raw_data_table,
                # 'created_at': metric.created_at,
                # 'updated_at': metric.updated_at,
                'to_analyze': metric.to_analyze,
                # 'calculated_data_table': metric.calculated_data_table,

                'data_source_id': metric.data_source_id,
                'data_source_name': metric.data_source.name,
                'data_source_location': metric.data_source.location,

                'component_id': metric.data_source.component.id,
                'component_name': metric.data_source.component.name,

                'asset_id': metric.data_source.component.asset.id,
                'asset_name': metric.data_source.component.asset.name
        }                   
            all_metric_info[metric.id] = metric_info

        return all_metric_info


    def _get_data_source_ids(self) -> List[int]:
        """
        Get data_source_id's whose metrics we analyse
        """
        data_source_ids = []
        for metric in self._metric_info.values():
            if metric['data_source_id'] not in data_source_ids:
                data_source_ids.append(metric['data_source_id'])

        return data_source_ids
