set analytics_dir="C:\Users\%USERNAME%\mhs\mhs-analytics\threshold_analytic"
set venv_root_dir="C:\Users\%USERNAME%\mhs\mhs-analytics\venv_ta"
cd %venv_root_dir%
call %venv_root_dir%\Scripts\activate.bat

cd %analytics_dir%
python main.py

cd %venv_root_dir%
call %venv_root_dir%\Scripts\deactivate.bat
cd %analytics_dir%
exit /B 1
