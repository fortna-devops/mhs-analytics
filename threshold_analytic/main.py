import json
from typing import Dict
import sys, getopt
import keyring

from analytic_manager import AnalyticManager
def main():
    db_host = keyring.get_password('system', 'db_host')
    db_name = keyring.get_password('system', 'db_name')
    db_username = keyring.get_password('system', 'db_username')
    db_password = keyring.get_password('system', 'db_password')
    db_schema = keyring.get_password('system', 'db_schema')

    try:
        AnalyticManager(db_host, db_name, db_username, db_password, db_schema).manage()
    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()
