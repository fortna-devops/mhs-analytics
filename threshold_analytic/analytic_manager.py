from datetime import datetime
import logging
from typing import List
import keyring

from calculators import DefaultCalculator
from ipc_rdb import Connector
from ipc_rdb.models import AnalyticsModel
from ipc_rdb import Analytics
from calculators.alarm_determiners import ThresholdAlarmDeterminer, RawDataAlarmDeterminer

logger = logging.getLogger()

if logger.handlers:
    logger.setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)


class AnalyticManager:
    """
    Analytic manager. Each analytic is performed by a Calculator object.
    """

    _alarm_determiner_map = {
        'default_threshold_analytic': ThresholdAlarmDeterminer,
        'no_agg_threshold_analytic': RawDataAlarmDeterminer,
        'linear_regression': 'blah',
     }

    def __init__(self, 
                 db_host: str, 
                 db_name: str,
                 db_username: str, 
                 db_password: str, 
                 db_schema: str):
            self.session = None
            self._set_session(db_host, db_name, db_username, db_password, db_schema)

    def _set_session(self, host: str, db_name: str, username: str, password: str, schema: str):
        connector = Connector(
            host=host,
            username=username,
            password=password
        )
        session = connector.get_session(db_name, schema)
        self.session = session

    def get_analytics(self) -> List[AnalyticsModel]:
        """
        Get analytics and their configurations
        """
        return Analytics(self.session).get_configs(active_status=True)

    def manage(self):
        """
        Manage executions of analytics (aka calculators)
        """

        # Timestamps from PLC are in local (ie, non-utc) mode
        timestamp = datetime.now()

        analytics = self.get_analytics()

        if len(analytics) ==0:
            logger.warning(f"Nothing to do. An analytic is not specified.")
            return

        for analytic in analytics:

            # BEGIN: for testing purpose ONLY 
            # if analytic.name == 'no_agg_threshold_analytic':
            #     timestamp = datetime.strptime('2021-07-02 16:21:19.398646', '%Y-%m-%d %H:%M:%S.%f')     # mhs_rt
            #     # timestamp = datetime.strptime('2021-12-02 15:14:33.490825', '%Y-%m-%d %H:%M:%S.%f')   # ups_savannah
            # elif analytic.name == 'default_threshold_analytic':
            #     timestamp = datetime.strptime('2021-12-02 15:14:33.490825', '%Y-%m-%d %H:%M:%S.%f')     # ups_savannah
            #     #timestamp = datetime.strptime('2021-05-01 01:01:01.161383', '%Y-%m-%d %H:%M:%S.%f')    # mhs_rt
            #     # END

            params = {
                'name': analytic.name,
                'session': self.session,
                'action': 'calculate',
                'amount_of_data_to_analyze': analytic.amount_data,      # expressed in seconds
                'alarm_determiner': self._alarm_determiner_map.get(analytic.name),
                'data_aggregation': analytic.data_aggregation,
                'timestamp': timestamp,
                }

            if params['alarm_determiner'] is None:
                logger.error(f"At least one alarm_determiner must be specified. Exiting.")
                return

            DefaultCalculator(self.session, params).calculate()
            