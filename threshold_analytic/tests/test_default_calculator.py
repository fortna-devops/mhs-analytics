# import pytest

# from ipc_rdb.models import ColorsEnum

# from calculators import DefaultCalculator
# from calculators.alarm_processors import BaseAlarmProcessor
# from calculators.alarm_processors import Result
# from generators import generate_metric, generate_metric_data, generate_alarm_processor_init_args


# @pytest.mark.parametrize(
#     'metric,data,result_metrics,alarm_processor_init_args',
#     [
#         (
#             generate_metric(1, name='rms_velocity_x'),
#             {},
#             {'no_threshold_level_names': [{'id': 1, 'name': 'rms_velocity_x'}]},
#             {}
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={'low_red': (1.0, ColorsEnum.RED)}),
#             {},
#             {'no_data': [{'id': 1, 'name': 'rms_velocity_x'}]},
#             {}
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={'low_red': (1.0, ColorsEnum.RED)}),
#             generate_metric_data(1, [''], [0]),
#             {'invalid_value_types': [{'id': 1, 'name': 'rms_velocity_x'}]},
#             {}
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={'low_red': (1.0, ColorsEnum.RED)}),
#             generate_metric_data(1, [1.1], [0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={'low_red': (1.0, ColorsEnum.RED)}),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(1.0, ColorsEnum.RED),
#                 high_yellow_level=(float('inf'), ColorsEnum.RED),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[1.1],
#                 minutes=[0]
#             )
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={'low_yellow': (2.0, ColorsEnum.YELLOW)}),
#             generate_metric_data(1, [2.0], [0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={'low_yellow': (2.0, ColorsEnum.YELLOW)}),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(float('inf'), ColorsEnum.RED),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[2.0],
#                 minutes=[0]
#             )
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={
#                 'low_red': (1.0, ColorsEnum.RED),
#                 'low_yellow': (2.0, ColorsEnum.YELLOW)
#             }),
#             generate_metric_data(1, [2.0], [0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={
#                     'low_red': (1.0, ColorsEnum.RED),
#                     'low_yellow': (2.0, ColorsEnum.YELLOW)
#                 }),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(float('inf'), ColorsEnum.RED),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[2.0],
#                 minutes=[0]
#             )
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={'high_yellow': (20.0, ColorsEnum.YELLOW)}),
#             generate_metric_data(1, [20.0], [0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={'high_yellow': (20.0, ColorsEnum.YELLOW)}),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(float('-inf'), ColorsEnum.RED),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[20.0],
#                 minutes=[0]
#             )
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={'high_red': (21.0, ColorsEnum.RED)}),
#             generate_metric_data(1, [21.0], [0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={'high_red': (21.0, ColorsEnum.RED)}),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(float('-inf'), ColorsEnum.RED),
#                 high_yellow_level=(21.0, ColorsEnum.RED),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[21.0],
#                 minutes=[0]
#             )
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={
#                 'high_yellow': (20.0, ColorsEnum.YELLOW),
#                 'high_red': (21.0, ColorsEnum.RED)
#             }),
#             generate_metric_data(1, [20.0], [0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={
#                     'high_yellow': (20.0, ColorsEnum.YELLOW),
#                     'high_red': (21.0, ColorsEnum.RED)
#                 }),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(float('-inf'), ColorsEnum.RED),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[20.0],
#                 minutes=[0]
#             )
#         ),
#         (
#             generate_metric(1, name='rms_velocity_x', threshold_levels={
#                 'low_red': (1.0, ColorsEnum.RED),
#                 'low_yellow': (2.0, ColorsEnum.YELLOW),
#                 'high_yellow': (20.0, ColorsEnum.YELLOW),
#                 'high_red': (21.0, ColorsEnum.RED)
#             }),
#             generate_metric_data(1, [2.0, 20.0], [1, 0]),
#             {},
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, name='rms_velocity_x', threshold_levels={
#                     'low_red': (1.0, ColorsEnum.RED),
#                     'low_yellow': (2.0, ColorsEnum.YELLOW),
#                     'high_yellow': (20.0, ColorsEnum.YELLOW),
#                     'high_red': (21.0, ColorsEnum.RED)
#                 }),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[2.0, 20.0],
#                 minutes=[1, 0]
#             )
#         )
#     ]
# )
# def test_process_metric_alarms(mocker, metric, data, result_metrics, alarm_processor_init_args):

#     class AlarmProcessor(BaseAlarmProcessor):

#         _type_name = 'test'

#     mocker.patch.object(DefaultCalculator, '__init__', return_value=None)
#     mocker.patch.object(AlarmProcessor, '__init__', return_value=None)
#     mocker.patch.object(AlarmProcessor, 'process', return_value=Result())

#     DefaultCalculator._alarm_processors = (
#         AlarmProcessor,
#     )

#     default_calculator = DefaultCalculator()
#     result = default_calculator._process_metric_alarms(metric, data)
#     assert result._metrics == result_metrics

#     if alarm_processor_init_args:
#         AlarmProcessor.__init__.assert_called_once_with(*alarm_processor_init_args)
