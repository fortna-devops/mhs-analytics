# import pytest

# from ipc_rdb.models import ColorsEnum

# from calculators.alarm_processors import BaseAlarmProcessor
# from generators import generate_alarm_processor_init_args, generate_metric, generate_sequence


# @pytest.mark.parametrize(
#     'base_alarm_processor_init_args,min_sequence_length,max_sequence_length,sequences',
#     [
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(1.0, ColorsEnum.RED),
#                 high_yellow_level=(float('inf'), ColorsEnum.RED),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[0.9, 1.0],
#                 minutes=[0, 1]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(1.0, ColorsEnum.RED, [0.9], 0, 0)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(float('inf'), ColorsEnum.RED),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[1.9, 2.0],
#                 minutes=[0, 1]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(2.0, ColorsEnum.YELLOW, [1.9], 0, 0)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(float('inf'), ColorsEnum.RED),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[0.9, 1.0, 1.9, 2.0],
#                 minutes=[0, 1, 2, 3]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(1.0, ColorsEnum.RED, [0.9], 0, 0),
#                 generate_sequence(2.0, ColorsEnum.YELLOW, [1.0, 1.9], 1, 2)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(float('-inf'), ColorsEnum.RED),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(float('inf'), ColorsEnum.RED),
#                 values=[20.1, 20.0],
#                 minutes=[0, 1]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(20.0, ColorsEnum.YELLOW, [20.1], 0, 0)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(float('-inf'), ColorsEnum.RED),
#                 high_yellow_level=(21.0, ColorsEnum.RED),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[21.1, 21.0],
#                 minutes=[0, 1]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(21.0, ColorsEnum.RED, [21.1], 0, 0)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(float('-inf'), ColorsEnum.RED),
#                 low_yellow_level=(float('-inf'), ColorsEnum.RED),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[20.1, 21.0, 21.1, 20.0],
#                 minutes=[0, 1, 2, 3]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(20.0, ColorsEnum.YELLOW, [20.1, 21.0], 0, 1),
#                 generate_sequence(21.0, ColorsEnum.RED, [21.1], 2, 2)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, min_value=0.8, max_value=21.2),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[0.8, 0.7, 0.9, 1.0, 1.9, 2.0, 20.0, 20.1, 21.0, 21.1, 21.3, 21.2],
#                 minutes=[0, 1, 2, 3, 5, 6, 7, 8, 10, 11, 14, 15]
#             ),
#             None,
#             None,
#             [
#                 generate_sequence(1.0, ColorsEnum.RED, [0.8], 0, 0),
#                 generate_sequence(1.0, ColorsEnum.RED, [0.9], 2, 2),
#                 generate_sequence(2.0, ColorsEnum.YELLOW, [1.0], 3, 3),
#                 generate_sequence(2.0, ColorsEnum.YELLOW, [1.9], 5, 5),
#                 generate_sequence(20.0, ColorsEnum.YELLOW, [20.1], 8, 8),
#                 generate_sequence(20.0, ColorsEnum.YELLOW, [21.0], 10, 10),
#                 generate_sequence(21.0, ColorsEnum.RED, [21.1], 11, 11),
#                 generate_sequence(21.0, ColorsEnum.RED, [21.2], 15, 15)
#             ]
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1, min_value=0.8, max_value=21.2),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[],
#                 minutes=[]
#             ),
#             None,
#             None,
#             []
#         ),
#         (
#             generate_alarm_processor_init_args(
#                 metric=generate_metric(1),
#                 low_red_level=(1.0, ColorsEnum.RED),
#                 low_yellow_level=(2.0, ColorsEnum.YELLOW),
#                 high_yellow_level=(20.0, ColorsEnum.YELLOW),
#                 high_red_level=(21.0, ColorsEnum.RED),
#                 values=[0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4],
#                 minutes=[0, 1, 2, 3, 4, 5, 6, 7]
#             ),
#             4,
#             4,
#             []
#         )
#     ]
# )
# def test_get_sequences(base_alarm_processor_init_args, min_sequence_length, max_sequence_length, sequences):

#     class AlarmProcessor(BaseAlarmProcessor):

#         _type_name = 'test'

#         _min_sequence_length = min_sequence_length

#         _max_sequence_length = max_sequence_length

#     alarm_processor = AlarmProcessor(*base_alarm_processor_init_args)
#     assert str(alarm_processor._get_sequences()) == str(sequences)
